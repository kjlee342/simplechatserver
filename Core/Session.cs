﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Core
{
    public class Session
    {
        public static readonly int HeaderSize = 2;
        public static readonly int PacketCodeSize = 2;

        Socket _socket;
        int _disconnected = 0;

        public byte[] _buffer = null;
        int _bufferLength = 0;
        int _readIndex = 0;
        int _writeIndex = 0;

        object _sendLock = new object();
        SocketAsyncEventArgs _sendEvent = new SocketAsyncEventArgs();
        Queue<byte[]> _sendQueue = new Queue<byte[]>();
        List<ArraySegment<byte>> _pendingList = new List<ArraySegment<byte>>();

        object _receiveLock = new object();
        SocketAsyncEventArgs _receiveEvent = new SocketAsyncEventArgs();

        public Session(int bytes)
        {
            _buffer = new byte[bytes];
            _bufferLength = bytes;
            _readIndex = 0;
            _writeIndex = 0;
            _disconnected = 0;
        }

        public void Initialize(Socket socket)
        {
            _socket = socket;

            _receiveEvent.Completed += new EventHandler<SocketAsyncEventArgs>(OnReceived);
            _sendEvent.Completed += new EventHandler<SocketAsyncEventArgs>(OnSent);

            RegisterReceive();
        }

        public virtual void OnConnected(EndPoint ep)
        {
        }

        public virtual void OnDisconnected(EndPoint ep)
        {
        }

        void Disconnect()
        {
            if (Interlocked.Exchange(ref _disconnected, 1) == 1)
            {
                return;
            }

            OnDisconnected(_socket.RemoteEndPoint);
            _sendQueue.Clear();
            _pendingList.Clear();

            _socket.Shutdown(SocketShutdown.Both);
            _socket.Close();
        }

        public void Send(List<byte[]> sendBufferList)
        {
            if (sendBufferList.Count == 0)
            {
                return;
            }

            lock (_sendLock)
            {
                for (int i = 0; i < sendBufferList.Count; i++)
                {
                    _sendQueue.Enqueue(sendBufferList[i]);
                }

                if (_pendingList.Count == 0)
                {
                    RegisterSend();
                }
            }
        }

        public void Send(byte[] sendBuff)
        {
            lock (_sendLock)
            {
                _sendQueue.Enqueue(sendBuff);
                if (_pendingList.Count == 0)
                {
                    RegisterSend();
                }
            }
        }

        void RegisterSend()
        {
            if (_disconnected == 1)
                return;

            while (0 < _sendQueue.Count)
            {
                _pendingList.Add(_sendQueue.Dequeue());
            }

            _sendEvent.BufferList = _pendingList;

            try
            {
                if (_socket.SendAsync(_sendEvent) == false)
                {
                    OnSent(null, _sendEvent);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"RegisterSend Failed {e}");
            }
        }

        void OnSent(object sender, SocketAsyncEventArgs e)
        {
            if (e.BytesTransferred <= 0 || e.SocketError != SocketError.Success)
            {
                Disconnect();
            }

            lock (_sendLock)
            {
                try
                {
                    e.BufferList = null;
                    _pendingList.Clear();

                    if (0 < _sendQueue.Count)
                    {
                        RegisterSend();
                    }
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception.Message);
                }
            }
        }

        void RegisterReceive()
        {
            if (_disconnected == 1)
            {
                return;
            }

            lock (_receiveLock)
            {
                ClearReceiveBuffer();
                _receiveEvent.SetBuffer(_buffer, _writeIndex, _bufferLength - _writeIndex);

                try
                {
                    if (_socket.ReceiveAsync(_receiveEvent) == false)
                    {
                        OnReceived(null, _receiveEvent);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        void OnReceived(object sender, SocketAsyncEventArgs e)
        {
            if (e.BytesTransferred <= 0 || e.SocketError != SocketError.Success)
            {
                Disconnect();
                return;
            }

            lock (_receiveLock)
            {
                if (_bufferLength <= e.BytesTransferred + _writeIndex)
                {
                    return;
                }
                _writeIndex += e.BytesTransferred;

                int processed = ProcessPacket();
                if (_writeIndex - _readIndex < processed)
                {
                    return;
                }

                _readIndex += processed;
            }

            RegisterReceive();
        }

        void ClearReceiveBuffer()
        {
            if (_writeIndex == _readIndex)
            {
                _writeIndex = 0;
            }
            else
            {
                int size = _writeIndex - _readIndex;
                Array.Copy(_buffer, _readIndex, _buffer, 0, size);

                _writeIndex = size;
            }

            _readIndex = 0;
        }

        int ProcessPacket()
        {
            int processLength = 0;
            int dataLength = _writeIndex - _readIndex;

            while (processLength < dataLength)
            {
                if (dataLength < HeaderSize)
                {
                    break;
                }

                ushort dataSize = BitConverter.ToUInt16(_buffer, _readIndex + processLength);
                if (dataLength < dataSize)
                {
                    break;
                }

                ushort packetCode = BitConverter.ToUInt16(_buffer, _readIndex + processLength + HeaderSize);
                ParsePacket(packetCode, new ArraySegment<byte>(_buffer, _readIndex + processLength + HeaderSize + PacketCodeSize, dataSize - 4));

                processLength += (dataSize);

            }

            return processLength;
        }

        public virtual void ParsePacket(ushort packetCode, ArraySegment<byte> bytes)
        {
        }
    }
}
