﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Core;

namespace TestClient
{
    public class ServerSession : Session
    {
        enum ServerPacketCode
        {
            playerID = 1,
            Chat,
            Alarm,
        }

        enum ClientPacketCode
        {
            Chat = 1,
            Alarm,
        }

        public int SessionID = 0;
        public bool IsConnected = false;

        public ServerSession(int bytes) : base(bytes)
        {
        }

        public override void OnConnected(EndPoint ep)
        {
            Console.WriteLine("connected to server");
            IsConnected = true;
        }

        public override void OnDisconnected(EndPoint ep)
        {
        }

        StringBuilder _sb = new StringBuilder();
        public override void ParsePacket(ushort packetCode, ArraySegment<byte> bytes)
        {
            _sb.Clear();
            switch ((ServerPacketCode)packetCode)
            {
                case ServerPacketCode.playerID:
                    {
                        int id = BitConverter.ToUInt16(bytes);
                        Console.WriteLine($"your id: {id}");
                    }
                    break;

                case ServerPacketCode.Chat:
                    {
                        int id = BitConverter.ToInt32(bytes.Array, bytes.Offset);
                        _sb.Append("player#");
                        _sb.Append(id);
                        _sb.Append(" : ");
                        _sb.Append(Encoding.UTF8.GetString(bytes.Array, bytes.Offset + sizeof(int), bytes.Count - sizeof(int)));

                        Console.WriteLine(_sb.ToString());
                    }
                    break;

                case ServerPacketCode.Alarm:
                    {
                        int id = BitConverter.ToInt32(bytes.Array, bytes.Offset);
                        int killed = BitConverter.ToInt32(bytes.Array, bytes.Offset + sizeof(int));

                        _sb.Append("player#");
                        _sb.Append(id);
                        _sb.Append(" game clear !\nKilled monster: ");
                        _sb.Append(killed);
                        Console.WriteLine(_sb.ToString());
                    }
                    break;
            }
        }

        public void Send(string chat)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(chat);
            Send((ushort)ClientPacketCode.Chat, bytes);
        }

        public void Send(ushort code, ArraySegment<byte> datas)
        {
            ushort size = (ushort)(datas.Count);

            byte[] sendBuffer = new byte[size + 4];
            Array.Copy(BitConverter.GetBytes((ushort)(size + 4)), 0, sendBuffer, 0, sizeof(ushort));

            Array.Copy(BitConverter.GetBytes(code), 0, sendBuffer, 2, sizeof(ushort));

            Array.Copy(datas.Array, datas.Offset, sendBuffer, 4, size);

            Send(sendBuffer);
        }
    }
}
