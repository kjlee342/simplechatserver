﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Core
{
    public class Listener
    {
        Socket _socket = null;
		Func<Session> _sessionMaker = null;

        public void Initialize(IPEndPoint ep, Func<Session> sessionMaker, int register = 10, int waiting = 100)
        {
            _socket = new Socket(ep.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

			_sessionMaker = sessionMaker;

            _socket.Bind(ep);
            _socket.Listen(waiting);

            for (int i = 0; i < register; i++)
            {
                SocketAsyncEventArgs args = new SocketAsyncEventArgs();
                args.Completed += new EventHandler<SocketAsyncEventArgs>(OnAcceptCompleted);
                RegisterAccept(args);
            }
        }

		void RegisterAccept(SocketAsyncEventArgs socketEvent)
		{
			socketEvent.AcceptSocket = null;

			if (_socket.AcceptAsync(socketEvent) == false)
            {
				OnAcceptCompleted(null, socketEvent);
			}
		}

		void OnAcceptCompleted(object sender, SocketAsyncEventArgs socketEvent)
		{
			if (socketEvent.SocketError != SocketError.Success)
			{
                Console.WriteLine("listner socket error");
				return;
			}

			Session session = _sessionMaker.Invoke();
			session.Initialize(socketEvent.AcceptSocket);
			session.OnConnected(socketEvent.AcceptSocket.RemoteEndPoint);

			RegisterAccept(socketEvent);
		}
	}
}
