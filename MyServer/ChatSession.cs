﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Core;

namespace MyServer
{
    public class ChatSession : Session
    {
        public int SessionID = 0;

        public ChatSession(int bytes) : base(bytes)
        {
        }

        public override void OnConnected(EndPoint ep)
        {
            byte[] buffer = new byte[sizeof(int)];
            if (BitConverter.TryWriteBytes(buffer, SessionID))
            {
                Send((ushort)ServerPacketCode.playerID, buffer);
            }
            Console.WriteLine($"player connected {SessionID}");
        }

        public override void OnDisconnected(EndPoint ep)
        {
            ChatServer.Instance.RemoveSession(this);
        }

        public override void ParsePacket(ushort packetCode, ArraySegment<byte> bytes)
        {
            switch ((ClientPacketCode)packetCode)
            {
                case ClientPacketCode.Chat:
                    ChatServer.Instance.BroadCastPacket((ushort)ServerPacketCode.Chat, MakePacket(bytes));
                    break;

                case ClientPacketCode.Alarm:
                    ChatServer.Instance.BroadCastPacket((ushort)ServerPacketCode.Alarm, MakePacket(bytes));
                    break;
            }
        }

        ArraySegment<byte> MakePacket(ArraySegment<byte> chatData)
        {
            ArraySegment<byte> newData = new ArraySegment<byte>(new byte[chatData.Count + sizeof(int)]);

            Array.Copy(BitConverter.GetBytes(SessionID), newData.Array, sizeof(int));
            Array.Copy(chatData.Array, chatData.Offset, newData.Array, sizeof(int), chatData.Count);

            return newData;
        }

        public void Send(ushort code, ArraySegment<byte> datas)
        {
            ushort size = (ushort)(datas.Count);

            byte[] sendBuffer = new byte[size + 4];
            Array.Copy(BitConverter.GetBytes((ushort)(size + 4)), 0, sendBuffer, 0, sizeof(ushort));

            Array.Copy(BitConverter.GetBytes(code), 0, sendBuffer, 2, sizeof(ushort));

            Array.Copy(datas.Array, datas.Offset, sendBuffer, 4, size);

            Send(sendBuffer);
        }
    }
}
