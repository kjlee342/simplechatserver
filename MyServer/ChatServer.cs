﻿using System;
using System.Collections.Generic;
using System.Text;
using Core;

namespace MyServer
{
	enum ServerPacketCode
    {
		playerID = 1,
		Chat,
		Alarm,
    }

	enum ClientPacketCode
	{
		Chat = 1,
		Alarm,
	}

	internal class ChatServer
    {
        static ChatServer _server = new ChatServer();
        public static ChatServer Instance { get { return _server; } }

		object _lock = new object();
        int _sessionID = 1;
		Dictionary<int, ChatSession> _sessions = new Dictionary<int, ChatSession>();

		public Session MakeSession()
		{
			lock (_lock)
			{

				ChatSession session = new ChatSession(1024 * 60);
				session.SessionID = _sessionID++;

				_sessions.Add(session.SessionID, session);

				return session;
			}
		}

		public void RemoveSession(ChatSession session)
		{
			lock (_lock)
			{
				_sessions.Remove(session.SessionID);
			}
		}

		public void BroadCastPacket(ushort code, ArraySegment<byte> bytes)
        {
			var iter = _sessions.GetEnumerator();
			while (iter.MoveNext())
			{
				iter.Current.Value.Send(code, bytes);
			}
		}
	}
}
