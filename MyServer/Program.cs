﻿using System;
using System.Net;
using System.Threading;
using Core;

namespace MyServer
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string host = Dns.GetHostName();
            IPHostEntry ipHost = Dns.GetHostEntry(host);
            IPAddress ipAddr = ipHost.AddressList[0];
            IPEndPoint ep = new IPEndPoint(ipAddr, 7777);

            Listener listener = new Listener();
            listener.Initialize(ep, () => { return ChatServer.Instance.MakeSession(); });

            while (true)
            {
                Thread.Sleep(1000);
            }
        }
    }
}
