﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Core
{
	public class Connector
    {
		Func<Session> _sessionMaker = null;

		public void Connect(IPEndPoint ep, Func<Session> sessionMaker)
		{
			Socket socket = new Socket(ep.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

			_sessionMaker = sessionMaker;

			SocketAsyncEventArgs args = new SocketAsyncEventArgs();
			args.Completed += OnConnectCompleted;
			args.RemoteEndPoint = ep;
			args.UserToken = socket;

			RegisterConnect(args);
		}

		void RegisterConnect(SocketAsyncEventArgs args)
		{
			Socket socket = args.UserToken as Socket;
			if (socket == null)
            {
				return;
			}

			if (socket.ConnectAsync(args) == false)
            {
				OnConnectCompleted(null, args);
			}
		}

		void OnConnectCompleted(object sender, SocketAsyncEventArgs args)
		{
			if (args.SocketError != SocketError.Success)
			{
                Console.WriteLine("connector socket error");
			}

			Session session = _sessionMaker.Invoke();
			session.Initialize(args.ConnectSocket);
			session.OnConnected(args.RemoteEndPoint);
		}
	}
}
