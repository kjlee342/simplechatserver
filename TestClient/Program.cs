﻿using System;
using System.Net;
using System.Threading;
using Core;

namespace TestClient
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string host = Dns.GetHostName();
            IPHostEntry ipHost = Dns.GetHostEntry(host);
            IPAddress ipAddr = ipHost.AddressList[0];
            IPEndPoint ep = new IPEndPoint(ipAddr, 7777);

            ServerSession clientSession = new ServerSession(1024);

            Connector connector = new Connector();
            connector.Connect(ep, () => { return clientSession; });

            while (clientSession.IsConnected == false)
            {
                Thread.Sleep(1000);
            }

            while (clientSession.IsConnected)
            {
                string chat = Console.ReadLine();
                clientSession.Send(chat);
            }
        }
    }
}
